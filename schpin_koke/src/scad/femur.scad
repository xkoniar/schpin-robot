include<LX15D.scad> include<tile.scad>

    t = 3;
w = 5;
screw_d = 3;
hole_d = 10;
plate_a = LX15D_horn_w;
support_r = 115;
support_rr = 8;

module femur_collision_a(tibiaOffset) translate([ 0, plate_a / 2 + w / 2, 0 ]) {
    hull() {
        cube([ Ta(T24), w, Ta(T24) ], center = true);
        translate([ 0, 0, tibiaOffset[2] ])
            cube([ Ta(T24), w, Ta(T24) ], center = true);
    }
    translate([ 0, 0, tibiaOffset[2] ]) hull() {
        cube([ Ta(T24), w, Ta(T24) ], center = true);
        translate([ tibiaOffset[0], 0, 0 ])
            cube([ Ta(T24), w, Ta(T24) ], center = true);
    }
}
module femur_collision_b(tibiaOffset) {
    mirror([ 0, 1, 0 ]) femur_collision_a(tibiaOffset);
}

module screw_pos(tibiaOffset) {
    rotate([ -90, 45, 0 ]) LX15D_horn_screw_pos() children();
    translate(tibiaOffset) rotate([ -90, 45, 0 ]) LX15D_horn_screw_pos()
        children();
}

module screw_center_pos(tibiaOffset) translate([ 0, plate_a / 2, 0 ]) {
    rotate([ -90, 0, 0 ]) children();
    translate(tibiaOffset) rotate([ -90, 0, 0 ]) children();
}

function middle_pos_f(tibiaOffset) = [ tibiaOffset[2], 0, tibiaOffset[2] ];
function v_len(v) = sqrt(pow(v[0], 2) + pow(v[1], 2) + pow(v[2], 2));

module middle_pos(tibiaOffset) {
    translate(middle_pos_f(tibiaOffset)) children();
}

module screw_base() { cylinder(d = screw_d + t * 2, h = w); }

module femur_plate(tibiaOffset)
    part("purple", part_name = str("femur_plate(", tibiaOffset, ")"),
         printed = true) union() {
    for (x = [0:Ta(T24):tibiaOffset[0] - 19])
        translate([ x, 0, tibiaOffset[2] ]) {
            rotate([ -90, 0, 0 ])
                tile(T24, h = w, center = true, centered_hole = true);
        }

    difference() {
        union() {
            hull() {
                translate([ 0, 0, -Ta(T24) / 2 + tibiaOffset[2] ])
                    cube([ Ta(T24) * 3 / 4, w, t ], center = true);
                rotate([ -90, 45, 0 ]) translate([ 0, 0, -w / 2 ])
                    LX15D_horn_screw_pos() screw_base();
            }

            hull() {
                translate(tibiaOffset + [ 0, -w / 2, 0 ]) rotate([ -90, 45, 0 ])
                    LX15D_horn_screw_pos() screw_base();
                translate(tibiaOffset + [ -19 / 2, 0, 0 ])
                    cube([ t, w, 24 * 3 / 4 ], center = true);
                minkowski() {
                    intersection() {
                        translate(tibiaOffset + [ 50, 0, 0 ])
                            cube([ 100, w - 1, 24 / 2 ], center = true);
                        rotate([ 90, 0, 0 ]) cylinder(r = support_r, h = w - 1,
                                                      center = true, $fn = 128);
                    }
                    rotate([ 90, 0, 0 ])
                        cylinder(r = support_rr, h = 1, center = true);
                }
            }
        }
        screw_pos(tibiaOffset) cylinder(d = screw_d, h = 100, center = true);
        screw_center_pos(tibiaOffset)
            cylinder(d = LX15D_horn_clearance_d, h = 100, center = true);
    }
}

module femur(tibiaOffset, expand = 0) assembly("femur") {
    for (i = [ -0.5, 0.5 ])
        translate([ 0, i * (plate_a + w / 2), 0 ]) assembly_direction(
            [ 0, i * 2, 0 ], expand) femur_plate(tibiaOffset);
    for (i = [ -0.5, 0.5 ])
        translate([ 0, i * (plate_a + w), 0 ]) screw_pos(tibiaOffset)
            rotate([ i * 180 + 90, 0, 0 ])
                assembly_direction([ 0, 0, -3 ], expand) Mscrew(M3L8C);
}
