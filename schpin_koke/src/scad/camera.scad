include <metric.scad>

module
D415_screws_pos()
{
    for (i = [ -1, 1 ])
        translate([ 0, i * (45 / 2), 0 ]) children();
}

D415_dim = [ 20.05, 99, 23 ];

module
D415() part("silver") difference()
{
    dim = D415_dim;
    hull() for (i = [ -1, 1 ])
        translate([ 0, i * (dim[1] / 2 - dim[2] / 2), 0 ]) rotate([ 0, 90, 0 ])
            cylinder(d1 = dim[2], d2 = dim[2] * 0.9, h = dim[0]);
    D415_screws_pos() rotate([ 0, 90, 0 ]) translate([ 0, 0, 3 - Ml(M3L8C) ])
        Mscrew_hole(M3L8C);
}

D415_stereo_fov = [ 69.4, 42.5 ];
D415_projector_fov = [ 80, 55 ]; // degrees, horizontal, vertical

module
D415_fov() color([ 0.5, 0.5, 0.5 ], alpha = 0.5)
{
    for (i = [ 0, 1 ])
        translate([ D415_dim[0], 20 - i * 55, 0 ])
            hull() for (j = [ -0.5, 0.5 ]) for (k = [ -0.5, 0.5 ]) rotate(
                [ 0, 90 + j * D415_stereo_fov[1], k * D415_stereo_fov[0] ])
                cylinder(d = 1, h = 200);

    translate([ D415_dim[0], 20 - 28, 0 ])
        hull() for (j = [ -0.5, 0.5 ]) for (k = [ -0.5, 0.5 ]) rotate(
            [ 0, 90 + j * D415_projector_fov[1], k * D415_projector_fov[0] ])
            cylinder(d = 1, h = 200);
}
