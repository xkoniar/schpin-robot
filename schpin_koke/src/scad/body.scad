include<LX15D.scad>;
include<LX15D/cables.scad>;
include<LX15D/tiles.scad>;
include<bat.scad>;
include<camera.scad>;
include<metric.scad>;
include<rpi_box.scad>;
include<tile.scad>;
include<util.scad>;

screw_d = 3;
t = 3;
middle_t = Ta(T24);

xplate_h = Ta(T24) / 2;
xplate_tile_h = 6;
xplate_w = Ta(T24) * 2;
xplate_l = Ta(T24) * 7;

module body_collision(positions) {
    hull() for (pos = positions) {
        translate(pos)
            cube([ LX15D_dim[1], LX15D_dim[1], LX15D_horn_w ], center = true);
    }
    cube([ xplate_l, xplate_w, xplate_h * 2 + xplate_tile_h * 2 ],
         center = true);
}

module spine() part("white", printed = true) {
    middle_h = 6;

    difference() {
        translate([ 0, 0, 0 ]) {
            for (i = [ -1, 1 ]) {
                for (x = [-Ta(T24):6:Ta(T24)])
                    translate([ x, Ta(T24) * i * 2, Ta(T24) / 2 ])
                        rotate([ 0, 90, 0 ]) hull()
                            tile_raw_plate(T24, 1, 1, 0.3);

                translate([ i * Ta(T24) * 7 / 2, 0, Ta(T24) / 2 ])
                    tile_box(T24);
                for (p = [
                         [ 5 / 2, 0 ], [ 3 / 2, 0 ], [ 3 / 2, 1 ], [ 3 / 2, -1 ]
                     ])
                    translate(
                        [ i * Ta(T24) * p[0], Ta(T24) * p[1], Ta(T24) / 2 ]) {
                        tile_box_base(T24);
                    }
            }

            hull() for (j = [ -1, 1 ]) for (i = [ -1, 1 ]) {
                translate([ i * Ta(T24) * 3 / 2, j * Ta(T24), 0 ])
                    tile_pos(T24, middle_h);
            }
            translate([ 0, 0, Ta(T24) - middle_h ])
                hull() for (j = [ -1, 1 ]) for (i = [ -1, 1 ]) {
                translate([ i * Ta(T24) * 3 / 2, j * Ta(T24), 0 ])
                    tile_pos(T24, middle_h);
                translate([ i * Ta(T24) * 1 / 2, j * Ta(T24) * 2, 0 ])
                    tile_pos(T24, middle_h);
            }
            for (i = [ -1, 1 ])
                for (j = [ -1, 1 ])
                    translate(
                        [ i * Ta(T24) / 2, j * Ta(T24) * 5 / 2, Ta(T24) / 2 ])
                        rotate([ j * 90, 0, 0 ]) tile_pos(T24, middle_h);
        }

        cube([ 2 * Ta(T24), Ta(T24), 100 ], center = true);
        translate([ 0, 0, middle_h + Ta(T24) / 2 ])
            cube([ 2 * Ta(T24), Ta(T24) * 3, Ta(T24) ], center = true);

        for (i = [ -1, 1 ])
            for (p = [
                     [ 5 / 2, 0 ], [ 3 / 2, 0 ], [ 3 / 2, 1 ], [ 3 / 2, -1 ]
                 ])
                translate([ i * Ta(T24) * p[0], Ta(T24) * p[1], Ta(T24) / 2 ]) {
                    tile_box_side_pos() cylinder(d = Ta(T24) / 2, h = Ta(T24));
                }

        for (j = [ -1, 1 ])
            for (i = [ -1, 1 ]) {
                translate([ Ta(T24) * i * 1 / 2, Ta(T24) * j * 1, 0 ])
                    tile_neg(T24, middle_h);
                translate([
                    Ta(T24) * i * 1 / 2, Ta(T24) * j * 2, Ta(T24) - middle_h / 2
                ]) {
                    tile_screw_neg(T24, middle_h);
                    tile_center_hole(T24, middle_h + 2, center = true);
                }
                translate([
                    Ta(T24) * i * 1 / 2, j * (Ta(T24) * 5 / 2 - middle_h / 2),
                    Ta(T24) / 2
                ]) rotate([ j * -90, 0, 0 ]) {
                    tile_screw_neg(T24, middle_h);
                    tile_center_hole(T24, middle_h + 2, center = true);
                }
            }

        for (j = [ -1, 1 ])
            for (i = [ -1, 1 ]) {
                for (p = [
                         [ 3 / 2, 1 ], [ 3 / 2, 0 ], [ 7 / 2, 1 ], [ 5 / 2, 0 ]
                     ])
                    translate([ Ta(T24) * p[0] * i, Ta(T24) * p[1] * j, 0 ])
                        tile_neg(T24, Ta(T24));
            }
    }
}

module bot_holder() part("red", part_name = "bot_holder", printed = true) {
    bot_t = 8;
    difference() {
        hull() {
            rotate([ 90, 0, 0 ]) tile_pos(T24, h = Ta(T24), center = true);
            for (i = [ -1, 1 ])
                translate([ i * Ta(T24), 0, Ta(T24) / 2 - bot_t ])
                    tile_pos(T24, h = bot_t, center = false);
        }
        rotate([ -90, 0, 0 ]) tile_neg(T24, Ta(T24), center = true);
        for (i = [ -1, 1 ])
            translate([ i * Ta(T24), 0, 0 ]) mirror([ 1, 0, 0 ]){
                tile_screw_pos(T24){
                    tile_screw_hole(T24, Ta(T24), centered_hole = false);
                    translate([0,0,Ta(T24)/2 - bot_t]) rotate([180, 0, 0]) cylinder(r=4,h=100);
                }
            tile_center_hole(T24, Ta(T24)+2, center = true);
    }
    }
}

module servo_pos(coord) {
    translate(coord) {
        if (coord[1] > 0) {
            rotate([ 0, 0, -90 ]) children();
        } else {
            rotate([ 0, 0, 90 ]) children();
        }
    }
}

module rpi4() part("green", part_name = "rpi4") {
    translate([ -85 / 2, -56 / 2, 0 ]) import("rpi4.stl");
}
module nucleo() part("white", part_name = "nucleo") {
    translate([ -43.2 / 2, -17.8 / 2, 0 ]) import("nucleo.stl");
}

module body(positions, expand = 0) {
    translate([ 0, -Ta(T24) * 3 / 2, Ta(T24) / 2 ]) rotate([ 0, 0, 90 ])
        bat_set();
    translate([ 0, Ta(T24) * 3 / 2, Ta(T24) / 2 ]) rotate([ 0, 0, 90 ])
        bat_set();

    for (x = [ -5 / 2, 5 / 2 ]) translate([ Ta(T24) * x, 0, 0 ]) bot_holder();

    for (pos = positions) servo_pos(pos) {
            rotate([ 180, 0, 0 ])
                LX15D_cable_holder_B(z_offset = 2, expand = expand);
            translate([ -5 * expand, 0, 0 ]) LX15D();
            // TODO: this pattern repeats, make a module with LX15D_u and some
            // set of screws also in: tibia
            translate(LX15D_U_offset + [ -4 * expand - LX15D_U_h, 0, 0 ]) {
                rotate([ 0, 90, 0 ]) tile_screw_pos(T24) Mscrew(M2L12C);
            }
            translate([ -2 * expand, 0, 0 ]) LX15D_U(T24, expand = expand);
        }

    translate([ -Ta(T24) * 3, 0, Ta(T24) * 1.5 + expand ])
        rotate([ 0, 0, 180 ]) {
        translate([ -Ta(T24) / 2, 0, 0 ]) tile_screw_pos(T24)
            rotate([ 180, 0, 0 ]) translate([ 0, 0, -2 - expand * 2 ])
                Mscrew(M2L8C);

        translate([ 0, 0, expand * 4 ]) top_rpi_holder();
        translate([ 0, 0, 7.5 + expand * 3 ]) rpi4();
        bot_rpi_holder();
        rpi_screw_pos() translate([ 0, 0, 3 - expand * 2.5 ]) Mscrew(M3L16C);
    }
    translate([ 0, 0, Ta(T24) / 2 ]) spine();
    //  translate([ 140, 0, 25 ]) { D415(); }
}
