include <metric.scad>
include <tile.scad>
include <util.scad>

module
pack_shape()
{
    k = 0.1;

    square([ 38 + 2 * k, 81 + 2 * k ], center = true);

    for (i = [ -1, 1 ])
        translate([ 0, i * (21 + 20) / 2, 0 ])
            square([ 43 + 2 * k, 20 + 2 * k ], center = true);

    for (i = [ -1, 1 ])
        translate([ i * (6 + 10) / 2, 0, 0 ])
            square([ 10 + 2 * k, 81 + 12 + 2 * k ], center = true);
}

power_dist_y = 78;
power_dist_x = [ -11, -6, 6, 11 ];

batt = [ 18, 65.0 ];

module
battery() part("red", part_name = "18650 battery")
{
    rotate([ 0, 90, 0 ]) cylinder(d = batt[0], h = batt[1], center = true);
}

module
bat_pcb() part("blue", part_name = "Bat PCB")
{
    linear_extrude(height = 2) pack_shape();
}

bat_holder_h = 5;

module
bat_set() union()
{
    bat_holder();
    translate([ 0, 0, -bat_holder_h ]) bat_pcb();
    for (i = [ -0.5, 0.5 ])
        translate([ i * batt[0], 0, -bat_holder_h - batt[0] / 2 ])
            rotate([ 0, 0, 90 ]) battery();
}

module
bat_holder() part("red", printed = true) translate([ 0, 0, -bat_holder_h ])
    difference()
{
    h = bat_holder_h;

    for (i = [-0.5:0.5])
        for (j = [-1.5:1.5])
            translate([ i * Ta(T24), j * Ta(T24), 0 ]) tile_pos(T24, h);

    linear_extrude(height = 3.5 * 2, center = true) pack_shape();

    for (i = [-0.5:0.5])
        for (j = [-1.5:1.5])
            translate([ i * Ta(T24), j * Ta(T24), 0 ]) tile_neg(T24, h);

    for (i = [ -0.5, 0.5 ])
        for (x = power_dist_x)
            translate([ x, i * power_dist_y, 0 ])
                cylinder(d = 3, h = 100, center = true);
}
