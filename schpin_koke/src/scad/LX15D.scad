include <metric.scad>
include <part.scad>
include <tile.scad>
include <util.scad>

LX15D_dim = [ 44.17, 22.95, 25.5 ];
LX15D_middle_t = 1.5; // height of the middle space
LX15D_middle_a = 3.0; // side space
LX15D_axis_offset = 10.66;
LX15D_horn_d = 20;
LX15D_horn_w = 38.6;
LX15D_screw_x = 41.2;
LX15D_screw_y = 18;
LX15D_horn_screw_d = 3;
LX15D_screw_clearance_d = 7;
LX15D_body_screw_d = 2;
LX15D_horn_clearance_d = 8;
LX15D_clearance_r = sqrt(pow(LX15D_dim[1] / 2, 2) + pow(LX15D_axis_offset, 2));

module
LX15D() part("gray", part_name = "LX15D servomotor") difference()
{
    dim = LX15D_dim;
    union()
    {
        translate([ dim[0] / 2 - LX15D_axis_offset, 0, 0 ])
        {
            cube(dim, center = true);
            difference()
            {
                cube(dim +
                         [
                             -LX15D_middle_a,
                             -LX15D_middle_a,
                             LX15D_middle_t * 2
                         ],
                     center = true);
                for (i = [ -0.5, 0.5 ])
                    for (j = [ -0.5, 0.5 ])
                        translate([ dim[0] * i, dim[1] * j, 0 ])
                            cylinder(d = 13, h = 100, center = true);
            }
        }

        cylinder(d = LX15D_horn_d, h = LX15D_horn_w, center = true);
    }
    for (i = [ -1, 1 ]) {
        LX15D_horn_screw_pos() translate([ 0, 0, i * LX15D_horn_w / 2 ])
            cylinder(d = LX15D_horn_screw_d, h = 10, center = true);
    }
    for (m = [ 0, 1 ])
        mirror([ 0, 0, m ]) for (i = [ -1, 1 ]) for (j = [ -1, 1 ])
        {
            LX15D_body_screw_pos(i, j)
                cylinder(d = LX15D_body_screw_d, h = 4, center = true);
        }
}

module
LX15D_collision()
{
    dim = LX15D_dim;

    translate([ dim[0] / 2 - LX15D_axis_offset, 0, 0 ])
    {
        cube(dim + [ 0, 0, LX15D_middle_t * 2 ], center = true);
    }

    cube([ LX15D_horn_d, LX15D_horn_d, LX15D_horn_w ], center = true);
}

module
LX15D_clearance()
{
    cylinder(r = LX15D_clearance_r, h = LX15D_horn_w, center = true);
}

module
LX15D_screw_plane()
{
    translate([ 0, 0, LX15D_dim[2] / 2 ]) children();
}

module LX15D_body_screw_pos(i, j)
    translate([ LX15D_dim[0] / 2 - LX15D_axis_offset, 0, 0 ])
{
    translate(
        [ i * LX15D_screw_x / 2, j * LX15D_screw_y / 2, LX15D_dim[2] / 2 ])
        children();
}

module
LX15D_bottom_screw_pos() for (j = [ -1, 1 ]) LX15D_body_screw_pos(1, j)
    children();

module
LX15D_horn_screw_pos()
{
    for (a = [0:90:360])
        rotate([ 0, 0, a ]) translate([ 14 / 2, 0, 0 ]) children();
}
module LX15D_side_plate(screw_d, holes, t) difference()
{
    union()
    {
        LX15D_bottom_screw_pos() hull()
        {
            cylinder(d = LX15D_screw_clearance_d, h = t);
            translate([ t * 2, 0, 0 ])
                cylinder(d = LX15D_screw_clearance_d, h = t);
        }
        hull() for (coord = holes)
        {
            translate(coord + [ 0, 0, LX15D_dim[2] / 2 ])
                cylinder(d = screw_d + t * 2, h = t);
        }
    }
    for (coord = holes)
        translate(coord) cylinder(d = screw_d, h = 100, center = true);
    LX15D_bottom_screw_pos()
        cylinder(d = LX15D_body_screw_d, h = 100, center = true);
}
