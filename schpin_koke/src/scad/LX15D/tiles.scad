include<../LX15D.scad>;
include<../part.scad>;

LX15D_U_h = 6;
LX15D_U_c = 0.2;
LX15D_U_offset = [ 40, 0, 0 ];
LX15D_U_screw_y = 16;

module LX15D_U_body(class)
    part("blue", part_name = str("LX15D_U_body(", Tname(class), ")"),
         printed = true) difference() {
    t = 2;
    translate([ -LX15D_U_h / 2, 0, 0 ])
        cube([ LX15D_U_h, Ta(T24), LX15D_dim[2] ], center = true);

    for (m = [ 0, 1 ])
        mirror([ 0, m, 0 ]) rotate([ 0, 90, 0 ]) tile_screw_pos(class = class)
            translate([ 0, 0, Mhead_h(M2L8C) - LX15D_U_h ]) Mscrew_hole(M2L8C);
    rotate([ 0, 90, 0 ])
        cylinder(d = Ta(T24) / 2, h = 100, center = true, $fn = 8);

    for (i = [ -0.5, 0.5 ])
        for (m = [ 0, 1 ])
            mirror([ 0, 0, m ]) translate([
                -LX15D_U_h / 2, i * LX15D_U_screw_y, -LX15D_dim[2] / 2 - t
            ]) {
                Mscrew_hole(M2L8C);
                translate([ 0, 0, 2 * t ]) Mnut_sidehole(M2N, l = 10);
            }
}
module LX15D_U_plate(class, t = 2)
    part("yellow", part_name = str("LX15D_U_plate(", Tname(class), ")"),
         printed = true) mirror([ 0, 0, 1 ]) difference() {
    intersection() {
        union() {
            translate(LX15D_U_offset -
                      [ LX15D_U_h / 2, 0, LX15D_dim[2] / 2 + t / 2 ])
                cube([ LX15D_U_h, Ta(T24), t ], center = true);
            mirror([ 0, 0, 1 ]) LX15D_bottom_screw_pos()
                hull() for (x = [ 0, LX15D_U_h / 2 ]) translate([ x, 0, 0 ])
                    cylinder(d = LX15D_screw_clearance_d, h = t);
        }
        cube([ 1000, Ta(class), 1000 ], center = true);
    }
    translate(LX15D_U_offset - [ LX15D_U_h / 2, 0, LX15D_dim[2] / 2 + t ]) {
        for (i = [ -0.5, 0.5 ])
            translate([ 0, i * LX15D_U_screw_y, 0 ]) Mscrew_hole(M2L8C);
    }
    LX15D_bottom_screw_pos()
        cylinder(d = LX15D_body_screw_d, h = 100, center = true);
}
module LX15D_U_plate_cable(class, t, s)
    part("yellow", part_name = str("LX15D_U_plate_cable(", Tname(class), ")"),
         printed = true) difference() {
    LX15D_U_plate(class, t = t + s / 2);
    translate(LX15D_U_offset -
              [ LX15D_U_h / 2, 0, LX15D_dim[2] / 2 + t + s / 2 ])
        cable_harness(w = LX15D_U_screw_y, s = s, r = t, l = LX15D_U_h);
}
module LX15D_U_plate_cable_lock(class, t, s)
    part("blue",
         part_name = str("LX15D_U_plate_cable_lock(", Tname(class), ",", t, ",",
                         s, ")"),
         printed = true) difference() {
    hull() translate(LX15D_U_offset -
                     [ LX15D_U_h / 2, 0, LX15D_dim[2] / 2 + t + s / 2 ]) {
        for (i = [ -0.5, 0.5 ])
            translate([ 0, i * LX15D_U_screw_y, 0 ]) rotate([ 180, 0, 0 ])
                cylinder(d = LX15D_U_h, h = t + s);
    }
    translate(LX15D_U_offset -
              [ LX15D_U_h / 2, 0, LX15D_dim[2] / 2 + 2 * t + s ]) {
        for (i = [ -0.5, 0.5 ])
            translate([ 0, i * LX15D_U_screw_y, 0 ]) Mscrew_hole(M2L8C);
    }
    for (z = [ 0, s + 2 * t ])
        translate(LX15D_U_offset -
                  [ LX15D_U_h / 2, 0, LX15D_dim[2] / 2 + t + s / 2 + z ])
            cable_harness(w = LX15D_U_screw_y, s = s, r = t, l = LX15D_U_h);
}
// this is meh, refactor
module LX15D_U(class, expand = 0) assembly("LX15D_U") union() {
    t = 2;
    cable_s = 2;

    translate(LX15D_U_offset) LX15D_U_body(class);

    translate([ 0, 0, expand ]) LX15D_U_plate(class, t = t);
    translate([ 0, 0, -expand ]) rotate([ 180, 0, 0 ])
        LX15D_U_plate_cable(class, t = t, s = cable_s);
    translate([ 0, 0, -2 * expand ])
        LX15D_U_plate_cable_lock(class, t = t, s = cable_s);

    translate(LX15D_U_offset) for (i = [ -0.5, 0.5 ]) translate([
        -LX15D_U_h / 2, i * LX15D_U_screw_y,
        -LX15D_dim[2] / 2 - 2 * t - cable_s - 2 * 2 *
        expand
    ]) Mscrew(M2L12C);

    translate(LX15D_U_offset) for (i = [ -0.5, 0.5 ]) translate([
        -LX15D_U_h / 2,
        i * LX15D_U_screw_y,
        LX15D_dim[2] / 2 + t + 2 * expand,
    ]) rotate([ 180, 0, 0 ]) Mscrew(M2L8C);

    LX15D_bottom_screw_pos() {
        translate([ 0, 0, -LX15D_dim[2] - 3 * expand - t ]) Mscrew(M2L8C);
        translate([ 0, 0, 3 * expand + t ]) rotate([ 180, 0, 0 ]) Mscrew(M2L8C);
    }

    for (i = [ -0.5, 0.5 ])
        for (j = [ -0.5, 0.5 ])
            translate(LX15D_U_offset + [
                expand - LX15D_U_h / 2, i * LX15D_U_screw_y,
                j * (LX15D_dim[2] - 2 * t)
            ]) rotate([ j * 180 + 90, 0, 0 ]) Mnut(M2N);
}

LX15D_BU_h = 6;
LX15D_BU_t = 3;
LX15D_BU_screw_a = 10;
LX15D_BU_offset = [ -25, 0, 0 ];
module LX15D_BU_plate() part("blue", printed = true) difference() {
    t = LX15D_BU_t;
    h = 5;
    horn_a = LX15D_horn_screw_d + t;
    bu_screw_a = Md(M2L8C) + t;
    translate([ 0, 0, h / 2 ]) hull() {
        LX15D_horn_screw_pos() cube([ horn_a, horn_a, h ], center = true);
        for (i = [ -0.5, 0.5 ])
            translate(LX15D_BU_offset +
                      [ LX15D_BU_h / 2, i * LX15D_BU_screw_a, 0 ])
                cube([ bu_screw_a, bu_screw_a, h ], center = true);
    }
    cylinder(d = LX15D_horn_clearance_d, h = 100, center = true, $fn = 8);
    LX15D_horn_screw_pos() translate([ 0, 0, t ]) rotate([ 180, 0, 0 ])
        Mscrew_hole(M3L5C);
    for (i = [ -0.5, 0.5 ])
        translate(LX15D_BU_offset + [ LX15D_BU_h / 2, i * LX15D_BU_screw_a, t ])
            rotate([ 180, 0, 0 ]) Mscrew_hole(M2L8C);
}
module LX15D_BU_body() part("yellow", printed = true)
    translate(LX15D_BU_offset + [ LX15D_BU_h / 2, 0, 0 ]) difference() {
    t = LX15D_BU_t;
    dim = [ LX15D_BU_h, LX15D_BU_screw_a + t * 2, LX15D_horn_w ];
    union() {
        rotate([ 0, 90, 0 ]) tile_pos(T24, LX15D_BU_h, center = true);
        cube(dim, center = true);
    }
    rotate([ 0, 90, 0 ]) tile_neg(T24, LX15D_BU_h, center = true);

    for (j = [ -1, 1 ])
        for (i = [ -0.5, 0.5 ])
            translate([ 0, i * LX15D_BU_screw_a, j * dim[2] / 2 ])
                rotate([ j * 90 + 90, 0, 0 ]) {
                Mscrew_hole(M2L8C);
                translate([ 0, 0, t ]) Mnut_sidehole(M2N, l = 10);
            }
}

module LX15D_BU(expand = 0) {
    t = LX15D_BU_t;
    plate_h = 5;
    LX15D_BU_body();
    for (an = [ 0, 180 ])
        rotate([ an, 0, 0 ]) translate([ 0, 0, LX15D_horn_w / 2 + expand ]) {
            LX15D_BU_plate();

            for (i = [ -0.5, 0.5 ])
                translate(LX15D_BU_offset +
                          [ LX15D_BU_h / 2, i * LX15D_BU_screw_a, t + expand ])
                    rotate([ 180, 0, 0 ]) Mscrew(M2L8C);

            LX15D_horn_screw_pos() translate([ 0, 0, plate_h + expand - t ])
                rotate([ 180, 0, 0 ]) Mscrew(M3L5C);
        }
    for (j = [ -1, 1 ])
        for (i = [ -0.5, 0.5 ])
            translate(LX15D_BU_offset + [
                LX15D_BU_h / 2 + expand, i * LX15D_BU_screw_a,
                j * LX15D_horn_w / 2
            ]) rotate([ j * 90 + 90, 0, 0 ]) {
                translate([ 0, 0, t ]) Mnut(M2N);
            }
}
