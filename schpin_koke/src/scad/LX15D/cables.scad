include<../LX15D.scad>;
include<../part.scad>;

module cable_harness(w, s, r, l) render() difference() {
	y = w / 2 - s / 2 - r;
	hull() for (i = [ -1, 1 ]) translate([ 0, i * y, 0 ])
	    rotate([ 0, 90, 0 ])
		cylinder(d = s + r * 2, h = l + 2, center = true);
	minkowski() {
		difference() {
			hull() for (i = [ -1, 1 ]) translate([ 0, i * y, 0 ])
			    rotate([ 0, 90, 0 ])
				cylinder(d = s + r * 2 + 0.1, h = l - r * 2,
					 center = true);
			hull() for (i = [ -1, 1 ]) translate([ 0, i * y, 0 ])
			    rotate([ 0, 90, 0 ]) cylinder(
				d = s + r * 2, h = l + 1, center = true);
		}
		sphere(r = r);
	}
}

module LX15D_cable_holder_B(z_offset, expand) {
	w = LX15D_dim[1];
	t = 2;
	h = 8;
	off = [ 30, 0, LX15D_dim[2] / 2 + z_offset ];
	screw_off = off + [ -2, 0, 0 ];
	screw_d = Md(M2L8C);
	screw_y_offset = w / 2 - screw_d / 2 - t;

	LX15D_cable_holder_B_screw_pos(screw_off, y_offset = screw_y_offset) {
		translate([ 0, 0, h - Mhead_h(M2L8C) + 4*expand ]) rotate([ 180, 0, 0 ])
		    Mscrew(M2L8C);
		translate([ 0, 0, -0.01 + expand ]) Mnut(M2N);
	}

	translate([0,0,1.5*expand])
	LX15D_cable_holder_B_body(part_i = 0, z_offset = z_offset, h = h, w = w,
				  t = t, off = off, screw_off = screw_off,
				  screw_d = screw_d,
				  screw_y_offset = screw_y_offset);
	translate([0,0,2*expand])
	LX15D_cable_holder_B_body(part_i = 1, z_offset = z_offset, h = h, w = w,
				  t = t, off = off, screw_off = screw_off,
				  screw_d = screw_d,
				  screw_y_offset = screw_y_offset);
}

module LX15D_cable_holder_B_screw_pos(off, y_offset) {
	for (i = [ -1, 1 ]) translate(off + [ 0, i * y_offset, 0 ]) children();
}

module LX15D_cable_holder_B_body(part_i, z_offset, h, w, t, off, screw_off,
				 screw_d, screw_y_offset) part("deeppink", printed=true)
    difference() {
	s = 3;
	r = 2;
	l = 10;
	harness_h = s + r * 2;

	translate(off + [ 0, 0, h / 2 ]) cube([ l, w, h ], center = true);
	for (i = [ -1, 1 ])
		LX15D_body_screw_pos(1, i) translate([ 0, 0, z_offset + t ])
		    rotate([ 180, 0, 0 ]) Mscrew_hole(M2L8C, hole_offset = 100);
	LX15D_cable_holder_B_screw_pos(screw_off, y_offset = screw_y_offset) {
		translate([ 0, 0, h - Mhead_h(M2L8C) ]) rotate([ 180, 0, 0 ])
		    Mscrew_hole(M2L8C);
		translate([ 0, 0, -0.01 ]) Mnut(M2N);
	}
	if (part_i == 0) {
		translate(off + [ 0, 0, 50 + harness_h / 2 ])
		    cube(100, center = true);
	}
	if (part_i == 1) {
		translate(off - [ 0, 0, 50 - harness_h / 2 ])
		    cube(100, center = true);
	}
	for (z = [ harness_h / 2, h ])
		translate(off + [ 0, 0, z ])
		    cable_harness(w - screw_d - t * 2, s, r, l);
}

module LX15D_cable_holder_A_screw_pos(off, screw_z) for (i = [ -1, 1 ])
    translate(off + [ 2, 0, i* screw_z ]) children();

module LX15D_cable_holder_A(expand) {
	l = 10;
	t = 2;
	h = 7;
	w = LX15D_dim[2] + t * 2;
	off = [ -LX15D_axis_offset + l / 2, LX15D_dim[1] / 2, 0 ];
	screw_z = w / 2 - t - Md(M2L8C) / 2;

	LX15D_cable_holder_A_screw_pos(off, screw_z) {
		translate([ 0, h + 3 * expand, 0 ]) rotate([ 90, 0, 0 ])
		    Mscrew(M2L8C);

		translate([ 0, h, 0 ]) rotate([ 90, 0, 0 ]) Mnut(M2N);
	}

	translate([ 0, expand, 0 ]) finalize("cyan")
	    LX15D_cable_holder_A_body(part_i = 0, l = l, w = w, t = t, h = h,
				      off = off, screw_z = screw_z);
	translate([ 0, 2 * expand, 0 ]) finalize("deeppink")
	    LX15D_cable_holder_A_body(part_i = 1, l = l, t = t, w = w, h = h,
				      off = off, screw_z = screw_z);
	for (m = [ 0, 1 ])
		mirror([ 0, 0, m ]) LX15D_body_screw_pos(-1, 1)
		    translate([ 0, 0, t + expand ]) rotate([ 180, 0, 0 ])
			Mscrew(M2L8C);
}

module LX15D_cable_holder_A_body(part_i, l, t, w, h, off, screw_z)
    difference() {
	s = 3;
	r = 2;
	k = 0.1;

	union() {
		translate(off + [ 0, h / 2, 0 ])
		    cube([ l, h, w ], center = true);
		for (m = [ 0, 1 ])
			mirror([ 0, 0, m ]) LX15D_body_screw_pos(-1, 1)
			    cylinder(d = LX15D_screw_clearance_d, h = t);
	}
	if (part_i == 0) {
		translate(off + [ 0, 50 + h / 2, 0 ]) cube(100, center = true);
	}
	if (part_i == 1) {
		translate(off - [ 0, 50 - h / 2, 0 ]) cube(100, center = true);
	}
	LX15D_body_screw_pos(-1, 1)
	    cylinder(d = LX15D_body_screw_d, h = 100, center = true);

	LX15D_cable_holder_A_screw_pos(off, screw_z) {
		translate([ 0, h, 0 ]) rotate([ 90, 0, 0 ]) Mscrew_hole(M2L8C);

		translate([ 0, -0.1, 0 ]) rotate([ -90, 0, 0 ]) Mnut(M2N);
	}

	translate(off + [ 0, s / 2 + r, 0 ]) rotate([ 90, 0, 0 ])
	    cable_harness(w - Md(M2L8C) * 2 - t * 2, s, r, l);
}

module LX15D_cable_holder_C(expand) {
	t = 2;
	s = 2;
	w = LX15D_dim[2] + t * 2;
	harness_w = w - t * 2 - Md(M2L8C);
	translate([ -3 * expand, 0, 0 ]) LX15D_cable_holder_C_lock(
	    t = t, s = s, w = w, harness_w = harness_w);
	translate([ -2 * expand, 0, 0 ]) LX15D_cable_holder_C_body(
	    t = t, s = s, w = w, harness_w = harness_w);
	translate([ -expand, 0, 0 ]) LX15D_cable_holder_C_screw_pos(t)
	    rotate([ 180, 0, 0 ]) Mnut(M2N);
	translate([ -4 * expand, 0, 0 ]) LX15D_cable_holder_C_screw_pos(t = t)
	    translate([ 0, 0, -Ml(M2L8C) ]) Mscrew(M2L8C);
}
module LX15D_cable_holder_C_screw_pos(t) {
	for (i = [ -1, 1 ])
		for (j = [ -1, 1 ])
			translate([
				-LX15D_axis_offset,  //
				i * (LX15D_dim[1] - t * 2 - Md(M2L8C)) / 2,
				j * (LX15D_dim[2] - t * 2 - Md(M2L8C)) / 2
			]) rotate([ 0, 90, 0 ]) children();
}
module LX15D_cable_holder_C_lock(t, s, w, harness_w) part("deeppink", printed=true)
    difference() {
	h = s + t * 2;
	l = Md(M2L8C) + t * 2;
	translate(
	    [ -LX15D_axis_offset, LX15D_dim[1] / 2 - t - Md(M2L8C) / 2, 0 ])
	    difference() {
		translate([ -h / 2 - s / 2 - t, 0, 0 ])
		    cube([ h, l, w ], center = true);
		for (x = [ 0, -s - t * 2 ])
			translate([ x - s / 2 - t, 0, 0 ]) rotate([ 90, 0, 90 ])
			    cable_harness(w = harness_w - t * 2, s = s, r = t,
					  l = l);
	}
	LX15D_cable_holder_C_screw_pos(t = t) translate([ 0, 0, -Ml(M2L8C) ])
	    Mscrew_hole(M2L8C);
}
module LX15D_cable_holder_C_body(t, s, w, harness_w) part("cyan", printed=true) difference() {
	l = LX15D_dim[1];
	h = s / 2 + t;
	union() {
		translate([ -h / 2 - LX15D_axis_offset, 0, 0 ])
		    cube([ h, l, w ], center = true);
		for (m = [ 0, 1 ])
			for (i = [ -1, 1 ])
				mirror([ 0, 0, m ]) LX15D_body_screw_pos(-1, i)
				    cylinder(d = LX15D_screw_clearance_d,
					     h = t);
	}
	LX15D_cable_holder_C_screw_pos(t = t) {
		translate([ 0, 0, -Ml(M2L8C) ]) Mscrew_hole(M2L8C);
		translate([ 0, 0, 0.001 ]) rotate([ 0, 180, 0 ]) Mnut(M2N);
	}
	translate([ -LX15D_axis_offset - s / 2 - t, 0, 0 ])
	    rotate([ 90, 0, 90 ])
		cable_harness(w = harness_w - t * 2, s = s, r = t, l = l);
	for (m = [ 0, 1 ])
		for (i = [ -1, 1 ])
			mirror([ 0, 0, m ]) LX15D_body_screw_pos(-1, i)
			    cylinder(d = LX15D_body_screw_d, h = 100,
				     center = true);
}
