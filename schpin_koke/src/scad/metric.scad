include <part.scad>

//    0  1      2      3  4       5       6     7              8
//    d, nut_d, nut_h, l, head_d, head_h, type, groove_offset, part_name
function M2gen(l, type, head_h, head_d, groove_offset, part_name) = //
    [ 2, 4.350, 1.600, l, head_d, head_h, type, groove_offset, part_name ];
function M3gen(l, type, head_h, head_d, groove_offset, part_name) = //
    [ 3, 6.300, 2.500, l, head_d, head_h, type, groove_offset, part_name ];

function Md(class) = class[0];
function Mnut_d(class) = class[1];
function Mnut_h(class) = class[2];
function Ml(class) = class[3];
function Mhead_d(class) = class[4];
function Mhead_h(class) = class[5];
function Mtype(class) = class[6];
function Mgroove_offset(class) = class[7];
function Mpart_name(class) = class[8];

M_FLAT = 0;
M_COLUMN = 1;
M_NUT = 2;

M2L4F = M2gen(4,
              M_FLAT,
              head_h = 1.2,
              head_d = 3.8,
              groove_offset = -1,
              part_name = "Flat M2x4 screw");
M2L12C = M2gen(12,
               M_COLUMN,
               head_h = 2,
               head_d = 3.98,
               groove_offset = 1,
               part_name = "Cylinder M2x12 screw");
M2L8C = M2gen(8,
              M_COLUMN,
              head_h = 2,
              head_d = 3.98,
              groove_offset = 1,
              part_name = "Cylinder M2x8 screw");
M2L6C = M2gen(6,
              M_COLUMN,
              head_h = 2,
              head_d = 3.98,
              groove_offset = 1,
              part_name = "Cylinder M2x6 screw");
M2L4C = M2gen(4,
              M_COLUMN,
              head_h = 2,
              head_d = 3.98,
              groove_offset = 1,
              part_name = "Cylinder M2x4 screw");
M2N = M2gen(0,
            M_NUT,
            head_h = 0,
            head_d = 0,
            groove_offset = 0,
            part_name = "M2 nut");

M3L16C = M3gen(16,
               M_COLUMN,
               head_h = 3,
               head_d = 5.22,
               groove_offset = 1,
               part_name = "Cylinder M3x16 screw");
M3L8C = M3gen(8,
              M_COLUMN,
              head_h = 3,
              head_d = 5.22,
              groove_offset = 1,
              part_name = "Cylinder M3x8 screw");
M3L5C = M3gen(5,
              M_COLUMN,
              head_h = 3,
              head_d = 5.22,
              groove_offset = 1,
              part_name = "Cylinder M3x5 screw");

module Mscrew_hole(class, hole_offset = 0)
{
    assert(Mtype(class) != M_NUT,
           "nut type should not be used with Mscrew_hole");
    cylinder(d = Md(class), h = Ml(class));
    if (Mtype(class) == M_FLAT) {
        translate([ 0, 0, -0.01 ])
            cylinder(d1 = Mhead_d(class), d2 = Md(class), h = Mhead_h(class));
    } else if (Mtype(class) == M_COLUMN) {
        rotate([ 180, 0, 0 ])
            cylinder(d = Mhead_d(class), h = Mhead_h(class) + hole_offset);
    }
}

module Mscrew(class) part("red", part_name = Mpart_name(class)) difference()
{
    assert(Mtype(class) != M_NUT, "nut type should not be used with Mscrew");
    union()
    {
        cylinder(d = Md(class), h = Ml(class));
        if (Mtype(class) == M_FLAT) {
            translate([ 0, 0, -0.01 ]) cylinder(
                d1 = Mhead_d(class), d2 = Md(class), h = Mhead_h(class));
        } else {
            rotate([ 180, 0, 0 ])
                cylinder(d = Mhead_d(class), h = Mhead_h(class));
        }
    }
    rotate([ 180, 0, 0 ])
    {
        translate([ 0, 0, Mgroove_offset(class) ])
            cylinder(d = Md(class), h = 100, $fn = 6);
    }
}

module Mnut(class) part("green", part_name = Mpart_name(class)) difference()
{
    assert(Mtype(class) == M_NUT, "only nut types should be used wtih Mnut");
    cylinder(d = Mnut_d(class), h = Mnut_h(class), $fn = 6);
    cylinder(d = Md(class), h = 100, center = true);
}

function Mnut_small_d(class) = sqrt(pow(Mnut_d(class), 2) -
                                    pow(Mnut_d(class) / 2, 2));

module Mnut_sidehole(class, l, k = 0.15)
{
    hull() for (x = [ 0, l ]) translate([ x, 0, 0 ])
        cylinder(d = Mnut_d(class) + 2 * k, h = Mnut_h(class) + 2 * k, $fn = 6);
    for (x = [ 0, l ])
        translate([ x, 0, Mnut_h(class) / 2 ])
            cube([ Md(class), Mnut_small_d(class), Mnut_h(class) + k * 4 ],
                 center = true);
}
