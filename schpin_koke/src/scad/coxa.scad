include <LX15D.scad>
include <LX15D/cables.scad>
include <LX15D/tiles.scad>

module coxa_collision(femurOffset)
{
    translate([ 5, 0, 0 ]) cube([ 40, 30, 50 ], center = true);

    translate([ femurOffset[0] / 2, 0, 0 ])
        cube([ femurOffset[0] + 25, 40, 30 ], center = true);
}

module
transition_pos() translate(LX15D_U_offset + [ -LX15D_U_h, 0, 0 ])
    rotate([ 0, -90, 0 ]) tile_screw_pos(T24) rotate([ 180, 0, 0 ]) children();
module
transition_nut_pos() translate([ 0, 0, LX15D_U_h + Tt(T24) ]) children();

module coxa(femurOffset, expand = 0)
{
    translate(femurOffset) rotate([ 90, 0, 180 ])
    {
        translate([ -3 * expand, 0, 0 ]) transition_pos()
            translate([ 0, 0, Mhead_h(M2L8C) ]) Mscrew(M2L8C);
        translate([ 2 * expand, 0, 0 ]) transition_pos() transition_nut_pos()
            Mnut(M2N);

        translate([ -2 * expand, 0, 0 ])
            LX15D_cable_holder_B(z_offset = 2, expand = expand);

        translate([ -4 * expand, 0, 0 ]) LX15D_cable_holder_C(expand = expand);

        translate([ -4 * expand, 0, 0 ]) LX15D();
        translate([ -2 * expand, 0, 0 ]) LX15D_U(T24, expand = expand);
    }
    rotate([ 0, 0, 180 ]) LX15D_BU(expand = expand);
}
