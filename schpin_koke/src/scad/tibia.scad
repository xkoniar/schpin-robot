include <LX15D.scad>
include <LX15D/cables.scad>
include <LX15D/tiles.scad>
include <metric.scad>
include <tile.scad>
include <util.scad>

t = 1.5;
screw_d = 2;
screw_e = 3;
screw_t = 1.5;
screw_step = 5;
b = 6;
leg_i = 10;

tile_h = 6;

bot_d = 40;
bot_screw_z = 6;
screw_m = Mhead_d(M2L4C);
mid_h = screw_m / 2 + bot_screw_z;

ball_j_w = 4;
ball_j_d = 7.3;
ball_j_l = 19 - ball_j_d / 2;
ball_angle_limit = 60;

leg_bot_screw_r = 16;

middle_tile_h = 5;

module
tip_cyli()
{
    cylinder(d = Ta(T24), h = Ta(T24), center = true);
    for (an = [0:5:360])
        rotate([ 0, 0, an ]) translate([ Ta(T24) / 2 - 0.2, 0, 0 ])
            cylinder(d = 1, h = Ta(T24), center = true);
}

tip_h = Ta(T24) / 2;

module
tip() part("yellow", printed = true) difference()
{
    intersection()
    {
        linear_extrude(height = 30) tile_relief(T24);
        rotate([ 90, 0, 0 ]) tip_cyli();
        rotate([ 0, 90, 0 ]) tip_cyli();
    }
    tile_screw_pos(T24)
    {
        cylinder(d = Tscrew_d(T24), h = 100, center = true);
        translate([ 0, 0, Tt(T24) ])
            cylinder(d = Tscrew_d(T24) + Tt(T24), h = 100);
    }
}

module
transition_pos() translate(LX15D_U_offset + [ -LX15D_U_h, 0, 0 ])
    rotate([ 0, -90, 0 ]) tile_screw_pos(T24) rotate([ 180, 0, 0 ]) children();

module
transition_nut_pos() translate([ 0, 0, LX15D_U_h + Tt(T24) ]) children();

module tip_pos(tipOffset) translate(tipOffset + [ -tip_h, 0, 0 ])
    rotate([ 0, 90, 0 ]) children();

module tibia(tipOffset, expand = 0)
{
    rotate([ 90, 0, 0 ])
    {
        LX15D_cable_holder_C(expand = expand);
        LX15D();
        translate([ 2.5 * expand, 0, 0 ]) LX15D_U(class = T24, expand = expand);
        translate([ expand, 0, 0 ]) transition_pos()
            translate([ 0, 0, Mhead_h(M2L8C) ]) Mscrew(M2L8C);
        translate([ 5 * expand, 0, 0 ]) transition_pos() transition_nut_pos()
            Mnut(M2N);
    }
    translate(LX15D_U_offset) rotate([ 0, 90, 0 ])
        translate([ 0, 0, 4 * expand ]) tile_H(T24, middle_tile_h, n = 2);
    tip_pos(tipOffset) translate([ 0, 0, 5 * expand ]) tip();

    tip_pos(tipOffset) tile_screw_pos(T24) translate([ 0, 0, t + 7 * expand ])
        rotate([ 180, 0, 0 ]) Mscrew(M2L6C);
    tip_pos(tipOffset) tile_screw_pos(T24)
        translate([ 0, 0, -2 * t + 3 * expand ]) rotate([ 180, 0, 0 ])
            Mnut(M2N);
}

module tibia_collision(tipOffset)
{
    dim = [ 64, 40, 26 ];
    dim2 = [ tipOffset[0], 6, 6 ];
    off = [ 20, 0, 0 ];

    hull()
    {
        translate(off) cube(dim, center = true);

        translate([ dim2[0] / 2, 0, 0 ]) cube(dim2, center = true);
    }
}

tip();
