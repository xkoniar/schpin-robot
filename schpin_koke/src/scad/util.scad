
module
axis() #color("pink")
{
    for (i = [-4:4]) {
        translate([ 0, 0, i * 20 ]) cylinder(d = 2, h = 10, center = true);
    }
}

module finalize(color_name) color(color_name) render() children();

module cornered_cube(dim, corner) hull()
{
    cube(dim + [ -corner * 2, 0, 0 ], center = true);
    cube(dim + [ 0, -corner * 2, 0 ], center = true);
}

module tube(d1, d2, h, center = false) difference()
{
    cylinder(d = d2, h = h, center = center);
    cylinder(d = d1, h = h * 3, center = true);
}
