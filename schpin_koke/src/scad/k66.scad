include<tile.scad> include<util.scad>

    screw_x = 55.88;
screw_y = 50;

dim = [ 81.28, 53.34 ];

t = 2;

module
k66f()
{
    r = 25.4 / 2;
    linear_extrude(height = t) minkowski()
    {
        circle(r = r);
        square(dim - [ r * 2, r * 2 ], center = true);
    }
}

module
k66_holder()
{
    for (i = [-0.5:0.5], j = [-1.5:1.5])
        translate([ j * Ta(T24), i * Ta(T24), 0 ]) tile(T24, h = t);
}

translate([ 0, 0, 5 ]) k66f();

k66_holder();
