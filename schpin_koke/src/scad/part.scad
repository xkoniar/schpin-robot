
module part(part_color = "red", part_name = undef, printed = false)
    color(part_color) render()
{
    // dummy moduel for smart parser :)
    children();
}

module assembly(assembly_name = undef)
{
    children();
}

module assembly_direction(dir, expansion)
{
    translate(dir*expansion) children();
}
