
module Package where

import           Parser
import           Schpin.Package
import           Schpin.Package.Xml
import           Schpin.SCAD
import           Schpin.Walker
import           Schpin.Walker.Pkg
import           Schpin.Walker.SCAD
import           Schpin.Walker.SRDF
import           Schpin.Walker.URDF
import           Schpin.Walker.Config
import           System.Directory               ( copyFile
                                                , createDirectory
                                                , createDirectoryIfMissing
                                                , getCurrentDirectory
                                                )

data ConfType = Default | Folded | Expanded deriving (Show, Eq);

kokePackageConfig :: String -> PackageConfig
kokePackageConfig cwd = PackageConfig
  { pkg_info    = PackageInfo
                    { pkg_name    = "schpin_koke"
                    , version     = PackageVersion 0 1 0
                    , description = "Data files for KOKE"
                    , maintainers = [Person "Jan Koniarik" "433337@mail.muni.cz"]
                    , license     = "LGPL"
                    , authors = [Person "Jan Koniarik" "433337@mail.muni.cz"]
                    , depends     = []
                    , exports = [BuildType "ament_cmake"]
                    }
  , scad_dir    = cwd ++ "/out/tmp/scad"
  , pkg_dir     = cwd ++ "/out/pkg/schpin_koke"
  , src_dir     = cwd ++ "/src/scad"
  }


exportExtras :: (ConfType -> SimpleWalker) -> IO ()
exportExtras w = do
  cwd <- getCurrentDirectory
  let conf = kokePackageConfig cwd
  let ew   = extendWalker (w Default) conf
  mapM_ (uncurry renderSCAD)
    $ [ (cwd ++ "/out/gen/skeleton/default.scad", getSCADVisualSkeleton ew)
      , ( cwd ++ "/out/gen/skeleton/default_col.scad"
        , getSCADCollisionSkeleton ew
        )
      , ( cwd ++ "/out/gen/skeleton/folded.scad"
        , getSCADVisualSkeleton (extendWalker (w Folded) conf)
        )
      , ( cwd ++ "/out/gen/skeleton/expanded.scad"
        , getSCADVisualSkeleton (extendWalker (w Expanded) conf)
        )
      ]

  generateBOM (ew) (conf)
  generatePrint (ew) (conf)


exportPackage :: (ConfType -> SimpleWalker) -> IO ()
exportPackage w = do
  cwd <- getCurrentDirectory
  let conf = kokePackageConfig cwd
  let ew   = extendWalker (w Default) conf

  generateWalkerPackage (w Default) conf
  writeCMakeLists conf
  writePackageXML conf 
  writeControlConfig ew conf
  generateManuals ew conf

  createDirectoryIfMissing False (pkg_dir conf ++ "/worlds")
  copyFile "worlds/koke.world"   (pkg_dir conf ++ "/worlds/koke.world")

