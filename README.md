Set of schpin packages specific for each robot.

## Koke

ROS package: [download](https://gitlab.fi.muni.cz/xkoniar/schpin-robot/-/jobs/artifacts/master/download?job=zipit)

To manually render:

  - goto schpin_koke folder
  - `cabal v2-run` executes the haskell script, this may take a while
    - make sure that you have `openscad` and `admesh` installed
  - `schpin_koke/out` folder contains generated files, use them in combination with `schpin_koke/src`
